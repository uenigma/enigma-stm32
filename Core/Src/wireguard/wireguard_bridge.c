// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 * wireguard_bridge.c
 *
 * Intefacing wireguard
 *
 *  Created on: Mar 17, 2020
 *      Author: movcmpret
 */
#include "wireguard_bridge.h"


void entry()
{
	wg_rpc_init(); // init RPC listener
	//wg_test_scenario();
}


void wg_calculate_params( u8* C, u8* publicKey, u8* hash, wg_params * out)
{
    u8 DHRes[BYTES_32];
    private_key_struct privKey;

    //Curve25519 ECDH
    get_private_key(0x0, &privKey); // get private key securely
    crypto_scalarmult_curve25519(DHRes,privKey.key,publicKey);
    memzero_explicit(&privKey, sizeof(private_key_struct)); // instantly zero out the stack region

    kdf2_result temp_res;
	//KDF2
    KDF2_32(C, DHRes, &temp_res);
    memcpy( out->ck, temp_res.t1, sizeof(temp_res.t1));

    u8 timestamp[12];
    create_tai64n_quick(timestamp);
    create_aead(temp_res.t2, timestamp , 12, hash, 32 , (u8*)&out->aead);
}


void HMAC(const u8* key, const size_t key_length, const u8* in, const size_t in_length, u8* out, const size_t out_length)
{
   // blake2s_hmac(out, in, key, out_length, in_length, key_length);
	size_t * inLength = (size_t*)malloc(sizeof(size_t)); // cause size_t inLength[1] would be too easy ;)
	memcpy(inLength, &in_length, sizeof(size_t));
	HMACn(key, key_length, in, inLength, 1, out, out_length);
	free(inLength);
}

void HMAC_32(const u8* key, const u8* in, u8* out)
{
	HMAC(key, 0x20, in, 0x20, out, 0x20);
}

void KDF1_32(const u8* key, const u8* in, u8* out)
{
	u8 t0[32];

	HMAC_32(key, in, t0);
	HMAC_32(t0, &ONE, out);
	memzero_explicit(t0, 0x20); // t0 is in stack, but lets assure the deletion
}

void KDF2_32(const u8* key, const u8* in, kdf2_result* out)
{
	u8 t0[32];

	HMAC_32(key, in, t0);
	HMAC(t0, 0x20, &ONE, 0x1,out->t1,0x20);

	// Create inputs
	u8 inSize = 2;
	size_t inLengths[] = {0x20,0x1};
	u8 hmacInput[inLengths[0] + inLengths[1]];

	// copy into input array
	memcpy(hmacInput, out->t1, inLengths[0]);
	memcpy(hmacInput + inLengths[0], &TWO, inLengths[1]);

	HMACn(t0, 0x20, hmacInput, inLengths, inSize, out->t2, 0x20);

	memzero_explicit(t0, 0x20); // vaues are in stack, but lets assure the deletion
}

/**
 * Copyright (C) 2015-2019 Jason A. Donenfeld <Jason@zx2c4.com>. All Rights Reserved.
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 */
void HMACn(const u8 *key, const size_t keylen, const u8 * inputs, const size_t * inputLenghts, size_t insize, u8 *out, const size_t outlen)
{

	struct blake2s_state state;
	u8 x_key[BLAKE2S_BLOCK_SIZE] __aligned(__alignof__(u32)) = { 0 };
	u8 i_hash[BLAKE2S_HASH_SIZE] __aligned(__alignof__(u32));
	int i;
	memcpy(x_key, key, keylen);

	for (i = 0; i < BLAKE2S_BLOCK_SIZE; ++i)
		x_key[i] ^= 0x36; // padding, see https://crypto.stackexchange.com/questions/3005/what-do-the-magic-numbers-0x5c-and-0x36-in-the-opad-ipad-calc-in-hmac-do

	blake2s_init(&state, BLAKE2S_HASH_SIZE);
	blake2s_update(&state, x_key, BLAKE2S_BLOCK_SIZE);
	//blake2s_update(&state, in, inlen);

	//*****
	// Hook here to inject multiple inputs
	// Iteration over the provided input array
	u8* newAddr = (u8*)inputs;
	blake2s_update( &state, inputs , inputLenghts[0] );
	for(int j = 1; j < insize; j++)
	{
		newAddr = newAddr + inputLenghts[j-1];
		blake2s_update(&state, newAddr, inputLenghts[j]);
	}
	//*****


	blake2s_final(&state, i_hash);

	for (i = 0; i < BLAKE2S_BLOCK_SIZE; ++i)
		x_key[i] ^= 0x5c ^ 0x36;

	blake2s_init(&state, BLAKE2S_HASH_SIZE);
	blake2s_update(&state, x_key, BLAKE2S_BLOCK_SIZE);
	blake2s_update(&state, i_hash, BLAKE2S_HASH_SIZE);
	blake2s_final(&state, i_hash);

	memcpy(out, i_hash, outlen);
	memzero_explicit(x_key, BLAKE2S_BLOCK_SIZE);
	memzero_explicit(i_hash, BLAKE2S_HASH_SIZE);
}

void create_aead(const u8* key, const u8* msg, const int msg_len, const u8* aad, const int aad_len, u8* out)
{
 u8 tag[16] = {0};
 u8 result[12] = {0};
 struct chachapoly_ctx ctx_out;
 chachapoly_init(&ctx_out, key, 256);
 chachapoly_crypt(&ctx_out, ZERO_NONCE, aad, aad_len,(u8*) msg, msg_len, result, tag, 16, 1);
 memcpy(out,result,12);
 memcpy(out+12,tag,16);

}


void wg_test_scenario()
{

	/// **************    EXAMPLES:
/*
	    const size_t outlen = 32;
	    const size_t keylen = 5;
	    const size_t inlen = 14;

	    //HMAC
	    u8 hash[outlen];
	    u8 key[] = {'h','e','l','g','a'};
	    u8 data[] = "shrimpsMitReis";
	    HMAC(key, keylen, data, inlen, hash, outlen);

	    // Curve25519
		u8 result[32];
		u8 privKey[] = {0x66,0xc8,0x08,0xe6,0xb5,0xbe,0x6d,0x66,0x20,0x93,0x4b,0xc6,0xff,0xa2,0xb8,0xb4,0x7f,0x97,0x86,0xc0,0x02,0xbf,0xb0,0x6d,0x53,0xa0,0xc2,0x75,0x35,0x64,0x1a,0x5d};
		u8 pubKey[]  = {0x7d,0x15,0x19,0x54,0x32,0xd1,0xac,0x7f,0x38,0xae,0xb0,0x54,0xd0,0x7d,0x9b,0x2e,0x1f,0xaa,0x91,0x3b,0x78,0xad,0x04,0xd5,0xef,0xdd,0x4a,0x1e,0xe8,0xd9,0xa3,0x19};
		crypto_scalarmult_curve25519(result, privKey, pubKey);

		*/

		wg_params* p = (wg_params*)(malloc(sizeof(wg_params)));
		memset(p, 0, sizeof(wg_params));
	    // Values coming from PC
	    u8 Ck[] = {0xf2, 0xea, 0xe8, 0xac, 0x34, 0x54, 0xb3, 0x9c, 0x3e, 0x9b, 0x7f, 0x3d, 0x33, 0x81, 0xc0, 0xaa, 0x18, 0x8d, 0x4d, 0x1f, 0xdd, 0x4a, 0x64, 0x30, 0x92, 0xe4, 0x43, 0xf1, 0x31, 0xed, 0xb, 0x87};
		u8 realPublicKey[] = {0xd6,0x8a,0x53,0x02,0xbe,0xca,0xdc,0xb3,0x76,0x89,0x22,0x09,0x72,0xf7,0xb5,0xaf,0xee,0xe5,0x36,0x7c,0x1f,0x8c,0x1e,0x42,0xa4,0x36,0x56,0xe0,0xb7,0xf5,0x9d,0x08};
		u8 hash[] = {0x4b, 0x19, 0xd2, 0x75, 0x7a, 0x79, 0x39, 0x80, 0x61, 0x1, 0xf4, 0x56, 0x53, 0x9c, 0x59, 0x8b, 0x16, 0x3b, 0x4c, 0xfb, 0xdc, 0xc7, 0x8b, 0x1, 0xc6, 0xf5, 0x11, 0x2f, 0x5d, 0x3e, 0x1, 0xfd};
		wg_calculate_params(Ck, realPublicKey, hash, p);

		/**
		 * Test timestamp:
           //u8 timestamp[] = {0x40, 0x0, 0x0, 0x0, 0x5e, 0x8d, 0x82, 0x5e, 0x34, 0xf5, 0x22, 0x45}; // Seal:  [c8, 41, 82, e5, 37, 24, dc, 59, c7, 48, fe, 6, 5b, 6e, 89, 4a, 3e, cc, 73, db, c6, ad, 99, b9, 5a, 82, f8, 4e]
		//Shall produce these outputs (taken from wireguard-rs client)

	       new CK (KDF2-1):   [9e, 24, f8, d3, e9, 68, 7d, 7d, a6, ab, 64, 66, e6, e6, f3, 89, 3, 83, ca, 10, 62, e3, c3, 35, e5, 16, a6, 60, 6d, 57, d1, 33]
		   key (KDF2-2):      [5d, cc, 95, f3, 15, fd, b0, fb, ed, 86, fe, 73, ab, 4b, 2d, ee, 21, 11, 9b, ea, 71, 92, 4d, f3, 98, 40, be, 66, d4, cd, 7c, bb]
		   seald timestamp:   [c8, 41, 82, e5, 37, 24, dc, 59, c7, 48, fe, 6, 5b, 6e, 89, 4a, 3e, cc, 73, db, c6, ad, 99, b9, 5a, 82, f8, 4e]
		 */



		free(p);
}



