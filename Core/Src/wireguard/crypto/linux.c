// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 * linux.c
 *
 *  Created on: Mar 17, 2020
 */

#include "../include/linux.h"

//see header for info. For now just calling memset
void *	memzero_explicit(void * addr, size_t size)
{
	return memset(addr,0, size);
}
