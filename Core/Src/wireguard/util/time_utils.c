// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 * time_utils.c
 *
 *  Created on: Apr 9, 2020
 */
#include "time_utils.h"

 RTC_HandleTypeDef time_utils_hrtc; //RTC Handle

void init_rtc_time(RTC_HandleTypeDef * hrtc)
{
		  HAL_PWR_EnableBkUpAccess();
		  __HAL_RCC_BKP_CLK_ENABLE(); //turn on the backup area clock
		  __HAL_RCC_PWR_CLK_ENABLE(); //turn on the power clock

		  hrtc->Instance = RTC;
		  hrtc->Init.AsynchPrediv = RTC_AUTO_1_SECOND;
		  hrtc->Init.OutPut = RTC_OUTPUTSOURCE_NONE;
		  if (HAL_RTC_Init(hrtc) != HAL_OK)
		  {
		    Error_Handler();
		  }

		  RTC_DateTypeDef DateToUpdate = {0};

		  // Get date values from 16 bit Backup register RTC_BKP_DR3 and RTC_BKP_DR4
			 uint32_t weekday_and_month_r = HAL_RTCEx_BKUPRead(hrtc, RTC_BKP_DR3);
			 uint32_t day_and_year_r = HAL_RTCEx_BKUPRead(hrtc, RTC_BKP_DR4);

			 memcpy(&DateToUpdate.WeekDay, &weekday_and_month_r, 2U );
			 memcpy(&DateToUpdate.Date, &day_and_year_r, 2U );

			 if (HAL_RTC_SetDate(hrtc, &DateToUpdate, RTC_FORMAT_BIN) != HAL_OK)
			 {
				    Error_Handler();
			 }
			 __HAL_RTC_SECOND_ENABLE_IT(hrtc,RTC_IT_SEC);  //turn on RTC clock seconds interrupt
			  RTC_TimeTypeDef time = {0};
			  HAL_RTC_GetTime(&time_utils_hrtc, &time, RTC_FORMAT_BIN);

}

void set_date_and_time(RTC_HandleTypeDef * hrtc, u8 year, u8 month, u8 day, u8 weekday, u8 hour, u8 minute, u8 second)
{
		  HAL_PWR_EnableBkUpAccess();
		  RTC_TimeTypeDef sTime = {0};
		  RTC_DateTypeDef DateToUpdate = {0};

		  sTime.Hours = hour;
		  sTime.Minutes = minute;
		  sTime.Seconds = second;

		  DateToUpdate.WeekDay = weekday; //RTC_WEEKDAY_WEDNESDAY;
		  DateToUpdate.Month = month; //RTC_MONTH_APRIL;
		  DateToUpdate.Date = day;
		  DateToUpdate.Year = year;

		  hrtc->Instance = RTC;
		  hrtc->Init.AsynchPrediv = RTC_AUTO_1_SECOND;
		  hrtc->Init.OutPut = RTC_OUTPUTSOURCE_NONE;

		  if (HAL_RTC_Init(hrtc) != HAL_OK)
		  {
		    Error_Handler();
		  }

		  if (HAL_RTC_SetTime(hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
		  {
		   Error_Handler();
		  }
		  set_date(hrtc, &DateToUpdate);
}

void set_date(RTC_HandleTypeDef * hrtc, RTC_DateTypeDef * DateToUpdate)
{

	  if (HAL_RTC_SetDate(hrtc, DateToUpdate, RTC_FORMAT_BIN) != HAL_OK)
	  {
		 Error_Handler();
	  }

	  //Store in 16 bit Backup register
	  u32 weekday_and_month;
	  u32 day_and_year;

	  memcpy(&weekday_and_month,&DateToUpdate->WeekDay,2U);
	  memcpy(&day_and_year,&DateToUpdate->Date,2U);

	  HAL_RTCEx_BKUPWrite(hrtc, RTC_BKP_DR3, weekday_and_month);
	  HAL_RTCEx_BKUPWrite(hrtc, RTC_BKP_DR4, day_and_year);
}


/*
 * Save the date at 00:00:01 and and 00:00:05
 * Gets called in second interrupt routine HAL_RTCEx_RTCEventCallback
 */
void date_backup_routine()
{
		  RTC_TimeTypeDef time = {0};
		  HAL_RTC_GetTime(&time_utils_hrtc, &time, RTC_FORMAT_BIN);
//		  if( time.Hours == 0 && time.Minutes == 0 && ( time.Seconds == 1  || time.Seconds == 5 )) // save date twice (@ 00:01 and 00:02)
//		  {
			  RTC_DateTypeDef date = {0};
			  HAL_RTC_GetDate(&time_utils_hrtc, &date, RTC_FORMAT_BIN);
				  //Store in 16 bit Backup register
			  u32 weekday_and_month;
			  u32 day_and_year;

			  memcpy(&weekday_and_month,&date.WeekDay,2U);
			  memcpy(&day_and_year,&date.Date,2U);

			  HAL_RTCEx_BKUPWrite(&time_utils_hrtc, RTC_BKP_DR3, weekday_and_month);
			  HAL_RTCEx_BKUPWrite(&time_utils_hrtc, RTC_BKP_DR4, day_and_year);
//		  }
}

//RTC Clock interrupt routine ( see: core/src/stm32f1xx_it.c -> RTC_IRQHandler.
//Calendar must be disabled in setup_rpoject.ioc!)
void HAL_RTCEx_RTCEventCallback(RTC_HandleTypeDef *hrtc)
{
	if(hrtc->Instance == RTC)
	{
		date_backup_routine();
	}
}


l64 get_unix_epoch(u8 year, u8 month, u8 day, u8 hour, u8 minute, u8 second){

	//Struct from time.h
	struct tm today;
	today.tm_year = year + 100; // years since 1900
	today.tm_mon = month -1;
	today.tm_mday = day;
	today.tm_hour = hour;
	today.tm_min = minute;
	today.tm_sec = second;

	time_t timestamp = mktime(&today);

	return timestamp;
}

/*
 * Copyright (c) 2018 Tim Kuijsten
 */
struct tai64n * nowtai64n(struct tai64n *out)
{
	RTC_TimeTypeDef currTime = {0};
	RTC_DateTypeDef currDate = {0};
    HAL_RTC_GetTime(&time_utils_hrtc, &currTime, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(&time_utils_hrtc, &currDate, RTC_FORMAT_BIN);
    u64 timestamp = get_unix_epoch(currDate.Year, currDate.Month, currDate.Date, currTime.Hours, currTime.Minutes, currTime.Seconds);
	out->sec = TAI0 + timestamp;
	//out->nano = 1000 * now.tv_usec + 500; //Dont need nano

	return out;
}

/*
 * Copyright (c) 2018 Tim Kuijsten
 * Convert a TAI64N label to external format.
 *
 * Return 0 on success, -1 on error with "out" unmodified.
 */
int externaltai64n(uint8_t *out, size_t outsize, const struct tai64n *in)
{
	uint64_t s;
	uint32_t n;

	if (in == NULL)
		return -1;

	if (outsize != 12)
		return -1;

	n = in->nano;

	out[11] = n & 255; n >>= 8;
	out[10] = n & 255; n >>= 8;
	out[9]  = n & 255; n >>= 8;
	out[8]  = n;

	s = in->sec;

	out[7] = s & 255; s >>= 8;
	out[6] = s & 255; s >>= 8;
	out[5] = s & 255; s >>= 8;
	out[4] = s & 255; s >>= 8;
	out[3] = s & 255; s >>= 8;
	out[2] = s & 255; s >>= 8;
	out[1] = s & 255; s >>= 8;
	out[0] = s;

	return 0;
}

void create_tai64n_quick(uint8_t *out)

{
	struct tai64n stamp;
	nowtai64n(&stamp);
	externaltai64n(out, 12, &stamp);
}

