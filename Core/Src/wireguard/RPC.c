// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 * RPC.c
 *
 *  Created on: Apr 28, 2020
 */

#include "RPC.h"

u8 pubkey[32];
u8 c[32];
u8 hash[32];
wg_params * params = NULL;
u8 send_buffer[60];


rpc_packet* last_packet; // saves the last packet
rpc_packet* current_packet; // saves the currently read packet
u16 failed_msg_id = 0; //last message which could not be used

rpc_call_info* current_rpc_call; // built rpc call from multiple packets

void (*callback_ptr)(rpc_call_info*); //callback function pointer, when message is successfully parsed

void rpc_init(void (*func_ptr)(rpc_call_info*))
{
	callback_ptr = func_ptr;
	init_vars();
}

void init_vars()
{
	last_packet = (rpc_packet*)malloc(sizeof(rpc_packet));
	memset(last_packet,0x0,sizeof(rpc_packet));

	current_packet = (rpc_packet*) malloc(sizeof(rpc_packet));
	memset(current_packet,0x0,sizeof(rpc_packet));

	current_rpc_call = (rpc_call_info*) malloc(sizeof(rpc_call));
	memset(current_packet,0x0,sizeof(rpc_call));
}

u8 validate_packet(rpc_packet* packet)
{
	 rpc_packet stack_pkt = {0};
	 memcpy(&stack_pkt, packet, sizeof(rpc_packet));
	 stack_pkt.crc32 = 0x00000000; //Set to 0x00000000
	 u32 crc32 = crc32_simple((u8*)&stack_pkt, sizeof(rpc_packet));

	return crc32 == packet->crc32? 1 : 0;

}

void rpc_respond(rpc_packet* packets_to_send, u8 amount)
{
	CDC_Transmit_FS(packets_to_send, amount * RPC_PACKET_SIZE);
}

/**
 * Sends an error message with the specified code and an optional payload
 * Provide a <48 byte payload
 */
void rpc_send_error_message(u8 msg_id, u16 code, u8* optional_payload, u8 payload_len)
{
	rpc_send_simple_answer(msg_id, code, optional_payload, payload_len);
}

/**
 * Sends an error message with the specified code and an optional payload
 * Provide a <48 byte payload
 */
void rpc_send_simple_answer(u8 msg_id, u16 code, u8* optional_payload, u8 payload_len)
{
	rpc_packet pkt[1] = {0};
	pkt[0].msg_id = msg_id;
	pkt[0].command = code;

	if( payload_len != 0)
	{
		memcpy(&pkt[0].body, optional_payload, payload_len);
		pkt[0].msg_len = payload_len;
		pkt[0].body_len = payload_len;
	}
	else
	{
		memset(&pkt[0].body, 0x0, RPC_PACKET_BODY_SIZE);
		pkt[0].body_len = 0;
		pkt[0].msg_len = 0;
	}
	pkt[0].crc32 = 0x00000000;

	pkt[0].crc32 = crc32_simple( (u8*)&pkt[0], sizeof(rpc_packet) );
	rpc_respond(pkt,1);

}

/**
 * Creates a void response with a custom response code (RPC_COMMAND)
 */
void rpc_create_void_response_packet(u16 msg_id, u16 code)
{
	rpc_send_simple_answer(msg_id, code, NULL, 0x0);
}

/**
 * creates an array of packets to be sent based on the rpc_call_info and the desired payload
 */
void rpc_create_response_packets(rpc_call_info * info, u8* payload, u8 payload_len, rpc_packet* out, u8 * out_amount)
{
	*out_amount = (int) (payload_len / RPC_PACKET_BODY_SIZE) + 1;

	for(int i = 0; i < *out_amount; i++)
	{
		int current_payload_size = payload_len - i * RPC_PACKET_BODY_SIZE > RPC_PACKET_BODY_SIZE ? RPC_PACKET_BODY_SIZE :  payload_len - i * RPC_PACKET_BODY_SIZE;
		out[i].msg_id = info->msg_id;
		out[i].command = info->command;
		out[i].crc32 = 0x00000000;
		out[i].msg_len = payload_len;
		out[i].msg_offset =  i ;
	    out[i].body_len = current_payload_size;
		memcpy(out[i].body, payload + i * RPC_PACKET_BODY_SIZE, current_payload_size);
		//calculate crc32
		out[i].crc32 = crc32_simple((u8*)&out[i], sizeof(rpc_packet));

	}
}

void rpc_call_builder()
{
	if( current_packet->msg_id != last_packet->msg_id ) // new message
	{
		if(current_rpc_call != NULL)
			free(current_rpc_call);
		current_rpc_call = (rpc_call_info*) malloc(sizeof(rpc_call_info));
		memset(current_rpc_call,0x0,sizeof(rpc_call)); // zero out
		current_rpc_call->msg_id = current_packet->msg_id;
		current_rpc_call->command = current_packet->command;
		current_rpc_call->payload_len = current_packet->msg_len;
		memcpy( current_rpc_call->payload, current_packet->body, current_packet->body_len);
	}
	else if( current_packet->msg_id == last_packet->msg_id ) // potential "next packet"
	{
		if( current_packet->msg_offset * RPC_PACKET_BODY_SIZE != last_packet->body_len + last_packet->msg_offset * RPC_PACKET_BODY_SIZE) // new offset must be the old offset plus old body length
		{
			// Error, Packet loss
			return;
		}
		//copy the message body to the location of the offset (payload+ ( offset * RPC_PACKET_BODY_SIZE)
		memcpy( current_rpc_call->payload + current_packet->msg_offset * RPC_PACKET_BODY_SIZE, current_packet->body, current_packet->body_len);
	}
	if( current_packet->msg_len == current_packet->body_len + current_packet->msg_offset * RPC_PACKET_BODY_SIZE) // is it the last packet?
			{
				callback_ptr(current_rpc_call);

				free(current_packet);
				free(last_packet);
				free(current_rpc_call);
				init_vars(); // reset values
			}
}


void usb_callback(u8* buf, u32 *len)
{

	if( *len == RPC_PACKET_SIZE )
	{
		HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_13);
		memcpy( last_packet, current_packet, RPC_PACKET_SIZE );
		memcpy( current_packet, buf, RPC_PACKET_SIZE );
		// if crc32 failed, or a later packet, associated with that message id was received
		if( validate_packet(current_packet) == 0 || failed_msg_id == current_packet->msg_id)
		{
			failed_msg_id = current_packet->msg_id;
			rpc_send_error_message(current_packet->msg_id, ERROR_CRC32_CHECK, NULL, 0x0);
			return;
		}
		rpc_call_builder();
	}
}


