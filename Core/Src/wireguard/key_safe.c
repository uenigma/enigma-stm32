// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 * key_safe.c
 *
 *  Created on: May 12, 2020
 */


#include "key_safe.h"

u8 is_auth = 0;

//**
// TODO: NEED EEPROM EMULATION HERE

const private_key_struct private_keys [4] = {
	/*ID, key*/	{0x0, {0x00, 0x00, 0x00, 0x00, 0x00, 0xe2, 0x98, 0x6e, 0x38, 0xd9, 0x26, 0x58, 0x41, 0x8a, 0x19, 0x67, 0x0b, 0xdc, 0x71, 0x7b, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xb0, 0x00, 0x00}},
		{0}, //empty
		{0},//empty
		{0}};//empty

const char passphrase[] = "1234";

void get_private_key(u32 id, private_key_struct* out)
{
	memcpy( out, &private_keys[id], sizeof(private_key_struct));
}

u8 authenticate_by_passphrase(const char * pass)
{
	if(!strcmp(passphrase,pass)) //if equal
		is_auth = 1;
	else
		is_auth = 0;
	return is_auth;
}

u8 is_authenticated()
{
	return is_auth;
}
