// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 * wireguard_RPC.c
 *
 *  Created on: Apr 28, 2020
 *
 */

#include "include/wireguard_RPC.h"

void wg_rpc_init()
{
	rpc_init(&wg_rpc_receive_callback); // init RPC listener with this callback function
}

/**
 * Function is called whenever a message is successfully received
 */
void wg_rpc_receive_callback(rpc_call_info* call_info)
{
	u8 out[512];
	u8 out_len;

	// "Unprivileged access"
	if(call_info->command == USER_AUTH)
	{
		authenticate(call_info);
		return;
	}
	else if( call_info->command == WG_GET_AEAD )
	{
		wg_get_aead(call_info->payload, out, &out_len);
		wg_rpc_response(call_info, out, out_len);
		return;
	}


	// Privileged area. only accessible by authentication
	// If not authenticated, return a packet
	if( !is_authenticated() )
	{
		rpc_create_void_response_packet(call_info->msg_id, ERROR_NOT_AUTHENTICATED);
		return;
	}

	if(call_info->command == TIME_SET){
		time_set(call_info->payload, out, &out_len);
	}
	else
	{
		//TODO error: command unknown packet
	}

	wg_rpc_response(call_info, out, out_len);
}

/**
 * Creates a response and calls the send function in RPC implementation
 */
void wg_rpc_response(rpc_call_info* call_info, u8* payload, u8 payload_len)
{
	rpc_packet out_packets[8] = {0}; //max 8
	u8 packet_amount = 0;
	rpc_create_response_packets(call_info, payload, payload_len, out_packets, &packet_amount);
	rpc_respond(out_packets, packet_amount);
}



/**
 *  RPC Functions
 */

void authenticate(rpc_call_info* call_info)
{
	char p[64] = {'\0'};
	memcpy(p, call_info->payload, call_info->payload_len); // pass + passlength
	if ( authenticate_by_passphrase((const char*)p) )
	{
		//success
		rpc_create_void_response_packet(call_info->msg_id, USER_AUTH);
	}
	else
	{
		//wrong pw
		rpc_create_void_response_packet(call_info->msg_id, ERROR_WRONG_PASSWORD);
	}
}

void change_password(const u8* password)
{
	//stub
}

void key_set(const u8 id)
{
	//stub
}

void key_save(const u8 id, const u8* key)
{
	//stub
}

void key_generate(const u8* pubkey_out)
{
	//stub
}

void key_pubkey_get(const u8 id)
{

}


void time_set( u8* payload, u8* out_payload, u8* out_payload_len ){

	time_t time;
	memcpy(&time, payload, 8); // u64 timestamp
	struct tm* t_time = localtime(&time);

	RTC_DateTypeDef date_to_update;
	RTC_TimeTypeDef time_to_update;

	date_to_update.Year = t_time->tm_year-100;
	date_to_update.Month = t_time->tm_mon+1;
	date_to_update.WeekDay = t_time->tm_wday;
	date_to_update.Date = t_time->tm_mday;

	time_to_update.Hours = t_time->tm_hour;
	time_to_update.Minutes = t_time->tm_min;
	time_to_update.Seconds = t_time->tm_sec;

	HAL_RTC_SetTime(&time_utils_hrtc, &time_to_update, RTC_FORMAT_BIN);
	HAL_RTC_SetDate(&time_utils_hrtc, &date_to_update, RTC_FORMAT_BIN);

	out_payload[0] = 1;
	*out_payload_len = 1;
}

void time_get(u32 * unix_epoch_out)
{

}

/**
 *  First 32 Bytes: Public Key
 *  Next 32 Bytes:	C
 *  Last 32 Bytes:  Hash
 */
void wg_get_aead( u8* payload, u8* out_payload, u8* out_payload_len ){
	u8* pubkey[32];
	u8* C[32];
	u8* hash[32];

	memcpy(pubkey, payload, 32);
	memcpy(C, payload+32, 32);
	memcpy(hash, payload+64, 32);
	wg_params params;
	wg_calculate_params(C, pubkey, hash, &params);
	*out_payload_len = sizeof(wg_params);
	// only reveal AEAD and KDF2-1(Ck). Keep the KDF2-2 (key) on the stm32
	memcpy(out_payload, &params, *out_payload_len);

}




