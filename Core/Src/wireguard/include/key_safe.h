/*
 * key_safe.h
 *
 *  Created on: May 12, 2020
 */

#ifndef SRC_WIREGUARD_INCLUDE_KEY_SAFE_H_
#define SRC_WIREGUARD_INCLUDE_KEY_SAFE_H_

#include "types.h"
#include "stdlib.h"
#include "string.h"

typedef struct private_key_struct {
	u8 id;
	u8 key[32];
}private_key_struct;

void get_private_key(u32 id, private_key_struct* out);

u8 authenticate_by_passphrase(const char * pass);

u8 is_authenticated();

#endif /* SRC_WIREGUARD_INCLUDE_KEY_SAFE_H_ */
