// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 * wireguard_RPC.h
 *
 *  Created on: Apr 28, 2020
 */

#ifndef SRC_WIREGUARD_INCLUDE_WIREGUARD_RPC_H_
#define SRC_WIREGUARD_INCLUDE_WIREGUARD_RPC_H_

#include "wireguard_bridge.h"

void wg_rpc_init();

void wg_rpc_receive_callback(rpc_call_info* call_info);

void wg_rpc_response(rpc_call_info* call_info, u8* payload, u8 payload_len);

void wg_create_void_response_packet(u16 msg_id, u16 status);

void authenticate(rpc_call_info* call_info);

void change_password(const u8* password);
void key_set(const u8 id);

void key_save(const u8 id, const u8* key);
void key_generate(const u8* pubkey_out);

void key_pubkey_get(const u8 id);

void time_set( u8* payload, u8* out_payload, u8* out_payload_len );

void time_get(u32 * unix_epoch_out);

void wg_get_aead( u8* payload, u8* out_payload, u8* out_payload_len );

#endif /* SRC_WIREGUARD_INCLUDE_WIREGUARD_RPC_H_ */
