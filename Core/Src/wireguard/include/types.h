// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 * types.h
 *
 *  Created on: Apr 7, 2020
 */

#ifndef SRC_WIREGUARD_INCLUDE_TYPES_H_
#define SRC_WIREGUARD_INCLUDE_TYPES_H_

#include <stdlib.h>
#include <stdbool.h>

// create typedefs for convenience
typedef __uint64_t u64;
typedef __uint32_t u32;
typedef __uint16_t u16;
typedef __uint8_t u8;
typedef size_t size_t;
typedef __int_least64_t l64;

/**
 * KDF2 result contains 2 values
 */
typedef struct kdf2_result_struct
{
	u8 t1[32];
	u8 t2[32];
}
kdf2_result;

/**
 * KDF2 result contains 2 values
 */
typedef struct wg_parameters
{
	u8 aead[28];
	u8 ck[32];
}
wg_params;

#endif /*SRC_WIREGUARD_INCLUDE_TYPES_H_*/
