// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 * time_utils.h
 *
 *  Created on: Apr 9, 2020
 */

#ifndef SRC_WIREGUARD_INCLUDE_TIME_UTILS_H_
#define SRC_WIREGUARD_INCLUDE_TIME_UTILS_H_
#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_rtc.h"
#include "stm32f1xx_hal_rtc_ex.h"
#include "stdio.h"
#include "types.h"
#include "time.h"


#define TAI0 ((uint64_t)1<<62)

struct tai64n {
	uint64_t sec;
	uint32_t nano; /* 0...999999999 */
};


extern RTC_HandleTypeDef time_utils_hrtc; //RTC Handle

/**
 * https://www.youtube.com/watch?v=zQ1sT9fjd3E // Tamper detection!
 * https://community.st.com/s/feed/0D50X00009XkW1nSAF
 *
 * Watch stm32f1xx_hal_rtc.c@RTC_Date_update. calls: set_date(hrtc, &hrtc->DateToUpdate); in the end
 *
 * Inits the time and date on startup
 */
void init_rtc_time(RTC_HandleTypeDef * hrtc);
/**
 * Sets the time and date to the RTC
 */
void set_date_and_time(RTC_HandleTypeDef * hrtc, u8 year, u8 month, u8 day, u8 weekday, u8 hour, u8 minute, u8 second);

/**
 * Sets the date to the RTC
 */
void set_date(RTC_HandleTypeDef * hrtc, RTC_DateTypeDef * DateToUpdate);

void date_backup_routine();

l64 get_unix_epoch(u8 year, u8 month, u8 day, u8 hour, u8 minute, u8 second);

struct tai64n * nowtai64n(struct tai64n *out);
/*
 * Copyright (c) 2018 Tim Kuijsten
 * Convert a TAI64N label to external format.
 *
 * Return 0 on success, -1 on error with "out" unmodified.
 */
int externaltai64n(uint8_t *out, size_t outsize, const struct tai64n *in);

/**
 * returns 12 byte tai64n timestamp
 */
void create_tai64n_quick(uint8_t *out);

#endif /* SRC_WIREGUARD_INCLUDE_TIME_UNTILS_H_ */
