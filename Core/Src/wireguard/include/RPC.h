// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 * RPC.h
 *
 *  Created on: Apr 28, 2020
 */

#ifndef SRC_WIREGUARD_INCLUDE_RPC_H_
#define SRC_WIREGUARD_INCLUDE_RPC_H_

#include "types.h"
#include "stm32f1xx_hal.h"
#include "linux.h"
#include "usbd_def.h"
#include "crc32.h"

typedef enum RPC_COMMANDS {
	USER_AUTH 				= 0x0010,
	USER_AUTH_CHANGE		= 0x0011,

	KEY_SAVE 				= 0x0020,
	KEY_GENERATE			= 0x0021,
	KEY_DELETE				= 0x0022,
	KEY_GET_PUBKEY			= 0x0023,

	TIME_READ				= 0x0030,
	TIME_SET				= 0x0031,

	WG_GET_AEAD				= 0x0040,

	//Error codes
	ERROR_NOT_AUTHENTICATED = 0xF010,
	ERROR_WRONG_PASSWORD	= 0xF011,

	ERROR_KEY_READ 			= 0xF020,
	ERROR_KEY_WRITE 		= 0xF021,

	//GENERAL
	ERROR_COMMAND_NOT_FOUND = 0xF0F0,
	ERROR_CRC32_CHECK		= 0xF0F1



} RPC_COMMANDS;

#define PAYLOAD_BUFFER_MAX 512 // Max bytes a message can have
#define RPC_PACKET_SIZE 64 //Do not change
#define RPC_PACKET_BODY_SIZE 48 //Do not change

typedef struct rpc_packet{
u16 msg_id; // per-packet increment
u16 msg_len; // length of the whole message (can be more than one packet size when split in multiple packets)
u8 body_len; //length of the current body in the packet
u8 msg_offset; // N-th packet counter
u16 command; //command according to struct in RPC.h

u32 spare;
u32 crc32; //CRC-32 Checksum

u8 body[RPC_PACKET_BODY_SIZE];

}rpc_packet;

typedef struct rpc_call_info{
u16 command;
u16 msg_id;
u16 payload_len;
u8 payload[PAYLOAD_BUFFER_MAX];
}rpc_call_info;


void rpc_init(void (*func_ptr)(rpc_call_info*));

void init_vars();

void rpc_call();

u8 validate_packet(rpc_packet* packet);

void rpc_respond(rpc_packet* packets_to_send, u8 amount);

void rpc_send_error_message(u8 msg_id, u16 code, u8* optional_payload, u8 payload_len);

void rpc_send_simple_answer(u8 msg_id, u16 code, u8* optional_payload, u8 payload_len);

void rpc_create_void_response_packet(u16 msg_id, u16 status);

void rpc_create_response_packets(rpc_call_info * info, u8* payload, u8 payload_len, rpc_packet* out, u8 * out_amount);

void usb_callback(u8* buf, u32 *len);


#endif /* SRC_WIREGUARD_INCLUDE_RPC_H_ */
