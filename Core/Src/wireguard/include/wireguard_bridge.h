// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 */
#ifndef WIREGUARD_BRIDGE_H_
#define WIREGUARD_BRIDGE_H_

#include "blake2s.h"
#include "curve25519.h"
#include "stdio.h"
#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "usbd_cdc_if.h"
#include "chachapoly.h"
#include "time_utils.h"
#include "RPC.h"
#include "wireguard_RPC.h"
#include "key_safe.h"


// *** Some Constants
static const u8 ONE = 0x1;
static const u8 TWO = 0x2;
static const u8 BYTES_32 = 0x20;

static const u8 ZERO_NONCE[12] = {0};


////// USB
typedef enum USB_ERRORS{
	USB_ERROR_INIT = 10,
	USB_ERROR_REGISTER_CLASS = 20,
	USB_ERROR_REGISTER_INTERFACE = 30,
	USB_ERROR_USB_START = 40} usb_err;

	void usb_callback(u8* Buf, u32 *Len);
	// Callback function, if USB throws and error
	void usb_error_handler(usb_err code);

	////////


void entry();

wg_params * get_new_wg_params();

/**
 * Stub for securely obtaining the static private key Spriv
 */
void wg_get_private_key(u8* out, u8 id);

/**
 *  Function, that is called with parameters coming from the wireguard client on the PC
 *  C: 		   The intermediate variable (key) used for Key Derivations
 *  publicKey: The peers static public key (Spub)
 */
void wg_calculate_params(u8* C,  u8* publicKey,  u8* hash, wg_params * out);
/**
 * Blake2s hashing algorithm used for HMAC and Key Derivation
 *  Calls HMACn with 1 input parameter
 */
void HMAC(const u8* key, const size_t key_length, const u8* in, const size_t in_length, u8* out, const size_t out_length);

/**
 * Taken from blake2 lib but extended with multiple input parameters.
 * Notes:
 *
 * inputs is an array of the input values.
 * Example: input1={0x2,0x45,0xF} input2 = {0x1}
 * 			You would construct following input paramters:
 * 			inputs = {0x2,0x45,0xF,0x1}
 * 			inputLengths = {3,1}
 * 			insize = 2
 */
void HMACn(const u8 *key, const size_t keylen, const u8 * inputs, const size_t * inputLenghts, size_t insize, u8 *out, const size_t outlen);
/**
 * Blake2s hashing algorithm used for HMAC and Key Derivation
 * 32 byte version (input-, output- and keylength)
 */

void HMAC32(const u8* key, const u8* in, u8* out);

void KDF1_32(const u8* key, const u8* in, u8* out);

void KDF2_32(const u8* key, const u8* in, kdf2_result* out);

/*
 *  Ceate AEAD. ChaChaPoly1305 encrypted hash/timestamp || tag
 */
void create_aead(const u8* key, const u8* msg, const int msg_len, const u8* aad, const int aad_len, u8* out);




void wg_test_scenario();

#endif
