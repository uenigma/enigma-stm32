// SPDX-License-Identifier: MIT
/*
 *
 * Copyright (C) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für Wissenschaftliche Datenverarbeitung mbH Göttingen
 *
 *
 * Functions taken from the linux kernel, to run wireguards crypto libs.
 *
 *  Created on: Mar 17, 2020
 */


#ifndef SRC_CRYPTO_WIREGUARD_LINUX_H_
#define SRC_CRYPTO_WIREGUARD_LINUX_H_

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "types.h"
#define U32_MAX		((u32)~0U)



#define DIV_ROUND_UP(n,d) (((n) + (d) - 1) / (d))
#define likely(x)    __builtin_expect(!!(x), 1) // processor prediction
#define unlikely(x)  __builtin_expect(!!(x), 0)

// Rotation functions on 32 bit values
#define ROL32(A, n) (((A) << (n)) | (((A) >> (32 - (n))) & ((1UL << (n)) - 1)))
#define ror32(A, n) ROL32((A), 32 - (n))

// Stubs
#define IS_ENABLED(a) ( 1 )

//adaption macros(non-linux)
#define TM_PRINTF(f_, ...) printf((f_), ##__VA_ARGS__)
#define WARN_ON(a) (a? TM_PRINTF("Warning triggered") : 0)


/**
 * https://www.kernel.org/doc/htmldocs/kernel-api/API-memzero-explicit.html
 *  usually using memset is just fine (!), but in cases where clearing out _local_ data
 *   at the end of a scope is necessary, memzero_explicit should be used instead in order
 *   to prevent the compiler from optimising away zeroing. memzero_explicit doesn't need
 *   an arch-specific version as it just invokes the one of memset implicitly.
 *
 *   Note: Could be prevented by disabling compiler optimization
 */
void *	memzero_explicit(void * addr, size_t size);

#endif /* SRC_CRYPTO_WIREGUARD_LINUX_H_ */
