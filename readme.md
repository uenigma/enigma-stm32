
# STM32 based security token for secure WireGuard authentication

Yet, there is no possibility to auhenticate with a WireGuard peer via a hardware security token. This code enables a STM32 to perform certain cryptographic operations with a 

# STM32F103C8T6 "Blue Pill" Setup
You will need and ST-Link V2 and a STM32F103C8Tx Board. 

Important: This code will not work, unless you integrate your own USB lib due to license issues. (Will be fixed soon)

## Quickstart 
1. Install the st-link driver (watch /driver/stlink for details)
2. Download and install the STM32CubeIDE https://www.st.com/en/development-tools/stm32cubeide.html
3. Create a new project and choose "STM32F103C8Tx"
4. Enable Serial Wire Debugging: Pinout&Configuration -> A-Z -> SYS -> Debug: Serial Wire
5. Safe project (code will be generated)
6. Add a debug configuration and chose in the Tab Debugger: Debug Probe: ST-LINK (ST-LINK GDB Server) and pick SWD Interface
7. Apply, then run or debug.


# Credits / License

Includes works from

- STMicroelectronics
- μNaCl - B. Haase, Endress, Hauser Conducta GmbH & Co. KG - https://munacl.cryptojedi.org/
- NaCl - Grigori Goronzy, D. J. Bernstein - https://nacl.cr.yp.to/
- Wireguard Project - Jason A. Donenfeld - https://wireguard.com

under different licenses. Check individual source files for more information.

If not stated otherwise, work is licensed under MIT license:

Copyright (c) 2020 Marjan Olesch <marjan.olesch AT gwdg.de>, GWDG - Gesellschaft für wissenschaftliche Datenverarbeitung mbH Göttingen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
